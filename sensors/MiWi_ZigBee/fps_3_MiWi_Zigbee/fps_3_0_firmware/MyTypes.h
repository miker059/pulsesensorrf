#include "ConfigApp.h"
//*********************************************************//
//*****  Status structure for Wireless Sensor *******//
typedef union _WS_STATUS_STRUCT
{
   unsigned char Val;
   struct{
        unsigned Module_Init:1;
        unsigned Send_Data:1;
		unsigned Measure:1;
        unsigned CurrentBuf:1;
		unsigned buf0overload:1;
		unsigned buf1overload:1;
		unsigned b6:1;
		unsigned b7:1;
         }Status;
} WS_STATUS_STRUCT;
typedef struct _ACTIVE_SCAN_REPORT
{
         unsigned char number;
         unsigned char Channel;
         unsigned char Address[MY_ADDRESS_LENGTH];
   
} ACTIVE_SCAN_REPORT;
