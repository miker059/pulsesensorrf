//**********************************************
#include <p18cxxx.h>
#include "ConfigApp.h"
#include "WirelessProtocols\MCHP_API.h" 
#include "MyTypes.h"
//**********************************************
#pragma config PLLDIV   = 6         // 24MHz Oscillator        
#pragma config CPUDIV   = OSC1_PLL2   
#pragma config USBDIV   = 2         // System clock 48MHz, 1 instruction time = 4cycle = 83ns
#pragma config FOSC     = INTOSCIO_EC//old HS //old HSPLL_HS
#pragma config FCMEN    = OFF
#pragma config IESO     = OFF
#pragma config PWRT     = OFF
#pragma config BOR      = ON
#pragma config BORV     = 3
#pragma config VREGEN   = OFF      //USB Voltage Regulator
#pragma config WDT      = OFF
#pragma config WDTPS    = 32768
#pragma config MCLRE    = ON
#pragma config LPT1OSC  = OFF
#pragma config PBADEN   = OFF
#pragma config CCP2MX   = OFF
#pragma config STVREN   = OFF
#pragma config LVP      = OFF
#pragma config XINST    = ON       // Extended Instruction Set
#pragma config CP0      = OFF
#pragma config CP1      = OFF
#pragma config CP2      = OFF
#pragma config CP3      = OFF
#pragma config CPB      = OFF
#pragma config CPD      = OFF
#pragma config WRT0     = OFF
#pragma config WRT1     = OFF
#pragma config WRT2     = OFF
#pragma config WRT3     = OFF
#pragma config WRTB     = OFF       // Boot Block Write Protection
#pragma config WRTC     = OFF
#pragma config WRTD     = OFF
#pragma config EBTR0    = OFF
//**********************************************
#define GreenLed            PORTBbits.RB5
#define GreenLedTris        TRISBbits.TRISB5 
#define BlueLed             PORTBbits.RB4
#define BlueLedTris         TRISBbits.TRISB4 
#define RedLed              PORTAbits.RA4
#define RedLedTris          TRISAbits.TRISA4 
#define SHDN_pin            PORTCbits.RC2
#define SHDN_pin_tris       TRISCbits.TRISC2 
#define RegEnTris           TRISAbits.TRISA5 
#define RegEn               PORTAbits.RA5  
#define INPUT_PIN           1
#define OUTPUT_PIN          0
#define LED_OFF             1
#define LED_ON              0  
//*********************************************        
#pragma udata
BYTE AdditionalNodeID[];
WS_STATUS_STRUCT WS_STATUS;
BYTE NetwMasterMAC[8];
BYTE Buf0[0x3C];
BYTE Buf1[0x3C];
BYTE Buf0Counter,Buf1Counter;
WORD MeasureIntervalConst;
#pragma idata 
BYTE myChannel = 25;
BYTE NumberOfMeasuring = 30;
BYTE SlotDuration = 60;
//*********************************************
extern void _startup (void); // See c018i.c in your C18 compiler dir
#pragma code _RESET_INTERRUPT_VECTOR = 0x000800
void _reset (void)
{
    _asm goto _startup _endasm
}
#pragma code
//*****************************************************
WORD CalculateMeasureIntervalConst(BYTE SlotCount)
{
    //Clock FREQ = 8 MHz add 1:8 PLL enabled, 1 timer increment = 4us , timeinterval2ms = number of 2ms intervals
    WORD frequency;
    if (SlotCount>131) return 0xFFDC;
    else
    {
       frequency=(GetInstructionClock()/1000)/8;// Prescaler = 8, 1000 - ������� � ������������
       return (0xFFFF-(SlotCount*SlotDuration*frequency/NumberOfMeasuring));
    }
}
//********************************************
void main(void)
{
    BYTE i,j;
    //��������� ����������� 
    OSCCON|=0xF2; 
    //all inputs as digital i/o 
    ADCON1|=0x0F; 
    //������������� ���������� �������
    WS_STATUS.Val=0x0000;
    WS_STATUS.Status.Module_Init=1;
    //������������� ��������� ��� �����������
    PHY_CS = 1;
    PHY_CS_TRIS = OUTPUT_PIN;
    PHY_RESETn_TRIS = OUTPUT_PIN;
    PHY_RESETn = 1;  
    PHY_WAKE_TRIS = OUTPUT_PIN;
    PHY_WAKE = 1;        
    RF_INT_TRIS = INPUT_PIN;  
    INTCON2bits.RBPU=0;     //Enable internall pullups for portb 
    SDI_TRIS = INPUT_PIN;
    SDO_TRIS = OUTPUT_PIN;
    SCK_TRIS = OUTPUT_PIN;
    SSPSTAT = 0xC0;
    SSPCON1 = 0x20;
    RCONbits.IPEN = 1;      // enable interrupts priority
    INTCON2bits.INTEDG2 = 0;// select INT2 interrupt effective edge
    INTCON3bits.INT2IF=0;    // clear  INT2 interrupt flag
    INTCON3bits.INT2IE=1;    // enable INT2 interrupt 
    INTCON3bits.INT2IP=1;    // INT2 interrupt priority = high
    //���������� ����������
    INTCONbits.GIE = 1;             //Enable all interrupts
    INTCONbits.PEIE=1;
    //������������� ���
    ADCON1=0x1B;//Vref+=2.5V ��� ������� ������ (ADCON1=0x1B) , Vref=Vdd ��� ������� ������� (ADCON1=0x0B)
                // AN0..AN3
    TRISAbits.TRISA3 = INPUT_PIN;
    TRISAbits.TRISA0 = INPUT_PIN;
    ADCON0=0;
    ADCON2=0xB9;
    ADCON0bits.ADON=1;
    PIR1bits.ADIF=0;
    PIE1bits.ADIE=1;
    //������������� �����������
    RegEnTris=OUTPUT_PIN;
    RegEn=1; 
    GreenLedTris=OUTPUT_PIN;
    GreenLed=LED_OFF;
    BlueLedTris=OUTPUT_PIN;
    BlueLed=LED_OFF;
    RedLedTris=OUTPUT_PIN;
    RedLed=LED_OFF;
    while(1)
    {
        if(TRUE==MiApp_MessageAvailable())
        {   
            switch (rxMessage.Payload[0])
            {
                case 0x01: //������ ���������
                            if (WS_STATUS.Status.Measure==1) break;
                            Buf1Counter=0;Buf0Counter=0;WS_STATUS.Status.CurrentBuf=0;
                            for(i=0;i<8;i++) NetwMasterMAC[i]=*(rxMessage.SourceAddress+i);//���������� ������ ������� ����
                            BlueLed=LED_OFF;
                            GreenLed=LED_ON;
                            WS_STATUS.Status.Measure=1;
                            MeasureIntervalConst=CalculateMeasureIntervalConst(rxMessage.Payload[1]);
                            PIR1bits.TMR1IF=0;
                            PIE1bits.TMR1IE=1;
                            T1CON=0x30;
                            TMR1H=MeasureIntervalConst>>8;
                            TMR1L=MeasureIntervalConst;
                            T1CONbits.TMR1ON=1;  
                            break;
                case 0x02: //��������� ���������
                            WS_STATUS.Status.Measure=0;
                            GreenLed=LED_OFF;
                            BlueLed=LED_ON;  
                            T1CONbits.TMR1ON=0;  
                            PIE1bits.TMR1IE=0;                        
                            break;
                case 0x03: //�������� ������
                            WS_STATUS.Status.Send_Data=1; 
                            break;  
               	default: break; 
            }
   	        MiApp_DiscardMessage();
       } 
      if (WS_STATUS.Status.Module_Init==1)
      {
           MiApp_ProtocolInit(FALSE);  
           MiApp_SetChannel(myChannel);
           MiApp_ConnectionMode(ENABLE_ALL_CONN);
           WS_STATUS.Status.Module_Init=0;
           BlueLed=LED_ON;
      }
      if (WS_STATUS.Status.Send_Data==1) 
      {
           MiApp_FlushTx();
           MiApp_WriteData(0x03);// ��� �������� ������
 		   if (TRUE==WS_STATUS.Status.Measure)
 		   {
                   
                    MiApp_WriteData(0x01);// ���������� ���������� �����
                    MiApp_WriteData(0x01);// ����� ���������� ����� (������ �� ��������������� �������� ������ ������ �� ����� �����)
                    WS_STATUS.Status.CurrentBuf=~WS_STATUS.Status.CurrentBuf;// ������������ �������
                    if (WS_STATUS.Status.CurrentBuf==1)
                    {
             	          if (WS_STATUS.Status.buf0overload==1)
                          {
                              MiApp_WriteData(0x1E);// ���������� ������ � 30 ����������;
             		          for (i=0;i<0x3C;i++)
             		          {
              		              MiApp_WriteData(Buf0[Buf0Counter]);
              			          Buf0Counter++;
                		          if (Buf0Counter==0x3C) Buf0Counter=0x00;
             		          }
                              WS_STATUS.Status.buf0overload=0;
                          }
                          else
                          {
                              MiApp_WriteData(Buf0Counter/2);// ���������� ���������
                              for (i=0;i<Buf0Counter;i++)	MiApp_WriteData(Buf0[i]);
                          } 
             	          Buf0Counter=0x00;
                   }
                   else
                   {
             	         if (WS_STATUS.Status.buf1overload==1)
                         {
                              MiApp_WriteData(0x1E);// ���������� ������ � 30 ����������;  
             		          for (i=0;i<0x3C;i++)
             		          {
              			         MiApp_WriteData(Buf1[Buf1Counter]);
              			         Buf1Counter++;
                	             if (Buf1Counter==0x3C) Buf1Counter=0x00;
             		          }
                              WS_STATUS.Status.buf1overload=0;
                         }
                         else
                         {
                              MiApp_WriteData(Buf1Counter/2);// ���������� ���������
                              for (i=0;i<Buf1Counter;i++)	MiApp_WriteData(Buf1[i]);
                         } 
                	     Buf1Counter=0x00;
                   } 
      	    } 
		    else
		    {
                    MiApp_WriteData(0x00);
		    } 
           if (TRUE==MiApp_UnicastAddress(&NetwMasterMAC[0],FALSE,FALSE)) GreenLed=~GreenLed;
           WS_STATUS.Status.Send_Data=0;   
       } // end if (WS_STATUS.Status.Send_Data==1) 
    }//end while
}//end main
//********************************************
void UserInterruptHandler(void)
{
 	if (PIR1bits.TMR1IF==1)//��������� ���������� ������� ���������� ���������
    {
  		T1CONbits.TMR1ON=0;
  		PIR1bits.TMR1IF=0;
        TMR1L=MeasureIntervalConst;  
        TMR1H=MeasureIntervalConst>>8;
  		T1CONbits.TMR1ON=1;
        if (ADCON0bits.GO_DONE==0) ADCON0bits.GO_DONE=1;
    }
    if (PIR1bits.ADIF==1)
    {
    			PIR1bits.ADIF=0;  
  				if (WS_STATUS.Status.CurrentBuf==1)
   				{
  			      Buf1[Buf1Counter]=ADRESL;
                  Buf1Counter++;
    	          Buf1[Buf1Counter]=ADRESH;
                  Buf1Counter++;
    		      if (Buf1Counter==0x3C) 
                  {
                      Buf1Counter=0x00;
                      WS_STATUS.Status.buf1overload=1;                   
                  }  
                }
                else
                {
   		 	      Buf0[Buf0Counter]=ADRESL;
                  Buf0Counter++;
  		          Buf0[Buf0Counter]=ADRESH;
                  Buf0Counter++;
    		      if (Buf0Counter==0x3C) 
                  {
                       Buf0Counter=0x00;
                       WS_STATUS.Status.buf0overload=1;
                  }
                }
     }
}
//*********************************************

