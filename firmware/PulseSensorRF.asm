
_InitMain:

;PulseSensorRF.c,71 :: 		void InitMain (){
;PulseSensorRF.c,74 :: 		OSCCON = 0x7A;   // 16MHz, INTOSC
	MOVLW      122
	MOVWF      OSCCON+0
;PulseSensorRF.c,76 :: 		TRISA = 0x0B; // 1011
	MOVLW      11
	MOVWF      TRISA+0
;PulseSensorRF.c,77 :: 		TRISC3_bit = 0;               // RC3 is output  in/off LED
	BCF        TRISC3_bit+0, BitPos(TRISC3_bit+0)
;PulseSensorRF.c,78 :: 		TRISC0_bit = 1;
	BSF        TRISC0_bit+0, BitPos(TRISC0_bit+0)
;PulseSensorRF.c,79 :: 		TRISC1_bit = 1;
	BSF        TRISC1_bit+0, BitPos(TRISC1_bit+0)
;PulseSensorRF.c,80 :: 		TRISC2_bit = 0;
	BCF        TRISC2_bit+0, BitPos(TRISC2_bit+0)
;PulseSensorRF.c,82 :: 		RC4PPS = 0x14;// TX pin on RC4
	MOVLW      20
	MOVWF      RC4PPS+0
;PulseSensorRF.c,83 :: 		RXPPS =  0x15;// RX pin on RC5
	MOVLW      21
	MOVWF      RXPPS+0
;PulseSensorRF.c,85 :: 		ANSELA = 0x03;
	MOVLW      3
	MOVWF      ANSELA+0
;PulseSensorRF.c,86 :: 		ADCON0 = 0x00; // select  AN0
	CLRF       ADCON0+0
;PulseSensorRF.c,87 :: 		ADCON1 = 0x02;// FOSC/64
	MOVLW      2
	MOVWF      ADCON1+0
;PulseSensorRF.c,88 :: 		ADCON2 = 0x00;
	CLRF       ADCON2+0
;PulseSensorRF.c,89 :: 		ADFM_bit=1;
	BSF        ADFM_bit+0, BitPos(ADFM_bit+0)
;PulseSensorRF.c,95 :: 		ANSELC = 0x07;
	MOVLW      7
	MOVWF      ANSELC+0
;PulseSensorRF.c,96 :: 		OPA1CON = 0x00;   // op amp disabled, inv input connected to OPA0IN,
	CLRF       OPA1CON+0
;PulseSensorRF.c,99 :: 		DAC1CON0 = 0x14; // DAC off, DAC out 2 pin, VREF on VREF+
	MOVLW      20
	MOVWF      DAC1CON0+0
;PulseSensorRF.c,101 :: 		PSA_bit=0;
	BCF        PSA_bit+0, BitPos(PSA_bit+0)
;PulseSensorRF.c,102 :: 		PS0_bit = 0; // 1MHZ timer clock
	BCF        PS0_bit+0, BitPos(PS0_bit+0)
;PulseSensorRF.c,103 :: 		PS1_bit = 0; // 1MHZ timer clock
	BCF        PS1_bit+0, BitPos(PS1_bit+0)
;PulseSensorRF.c,104 :: 		PS2_bit = 0; // 1MHZ timer clock
	BCF        PS2_bit+0, BitPos(PS2_bit+0)
;PulseSensorRF.c,105 :: 		TMR0CS_bit=0;
	BCF        TMR0CS_bit+0, BitPos(TMR0CS_bit+0)
;PulseSensorRF.c,106 :: 		TMR0IE_bit=1;
	BSF        TMR0IE_bit+0, BitPos(TMR0IE_bit+0)
;PulseSensorRF.c,107 :: 		GIE_bit = 1;
	BSF        GIE_bit+0, BitPos(GIE_bit+0)
;PulseSensorRF.c,109 :: 		TMR1CS0_bit = 0; // Fosc/4
	BCF        TMR1CS0_bit+0, BitPos(TMR1CS0_bit+0)
;PulseSensorRF.c,110 :: 		TMR1CS1_bit = 0; // Fosc/4
	BCF        TMR1CS1_bit+0, BitPos(TMR1CS1_bit+0)
;PulseSensorRF.c,111 :: 		T1CKPS0_bit = 1; // 1:8 prescaler
	BSF        T1CKPS0_bit+0, BitPos(T1CKPS0_bit+0)
;PulseSensorRF.c,112 :: 		T1CKPS1_bit = 1; // 1:8 prescaler
	BSF        T1CKPS1_bit+0, BitPos(T1CKPS1_bit+0)
;PulseSensorRF.c,113 :: 		PEIE_bit=1;
	BSF        PEIE_bit+0, BitPos(PEIE_bit+0)
;PulseSensorRF.c,114 :: 		TMR1IE_bit=1;
	BSF        TMR1IE_bit+0, BitPos(TMR1IE_bit+0)
;PulseSensorRF.c,116 :: 		TX1STA = 0x00;
	CLRF       TX1STA+0
;PulseSensorRF.c,117 :: 		BRGH_bit = 1;
	BSF        BRGH_bit+0, BitPos(BRGH_bit+0)
;PulseSensorRF.c,118 :: 		TXEN_bit=1;
	BSF        TXEN_bit+0, BitPos(TXEN_bit+0)
;PulseSensorRF.c,119 :: 		RC1STA=0x00;
	CLRF       RC1STA+0
;PulseSensorRF.c,120 :: 		SREN_bit=1;
	BSF        SREN_bit+0, BitPos(SREN_bit+0)
;PulseSensorRF.c,121 :: 		SPEN_bit=1;
	BSF        SPEN_bit+0, BitPos(SPEN_bit+0)
;PulseSensorRF.c,123 :: 		SP1BRGL = 8;     //config 115200 at fosc=16MHz
	MOVLW      8
	MOVWF      SP1BRGL+0
;PulseSensorRF.c,125 :: 		UART1_Init(115200);
	BSF        BAUDCON+0, 3
	MOVLW      34
	MOVWF      SPBRG+0
	MOVLW      0
	MOVWF      SPBRG+1
	BSF        TXSTA+0, 2
	CALL       _UART1_Init+0
;PulseSensorRF.c,127 :: 		LED=1;
	BSF        LATC3_bit+0, BitPos(LATC3_bit+0)
;PulseSensorRF.c,130 :: 		SLEEP_IO=1;
	BSF        LATA4_bit+0, BitPos(LATA4_bit+0)
;PulseSensorRF.c,132 :: 		LATC3_bit=0;
	BCF        LATC3_bit+0, BitPos(LATC3_bit+0)
;PulseSensorRF.c,133 :: 		TRISA = 0x0B;
	MOVLW      11
	MOVWF      TRISA+0
;PulseSensorRF.c,134 :: 		oldstate=0;
	BCF        _oldstate+0, BitPos(_oldstate+0)
;PulseSensorRF.c,136 :: 		IOCAP3_bit=1;
	BSF        IOCAP3_bit+0, BitPos(IOCAP3_bit+0)
;PulseSensorRF.c,137 :: 		IOCIE_bit=1;
	BSF        IOCIE_bit+0, BitPos(IOCIE_bit+0)
;PulseSensorRF.c,140 :: 		}
L_end_InitMain:
	RETURN
; end of _InitMain

_interrupt:

;PulseSensorRF.c,141 :: 		void interrupt ()        //
;PulseSensorRF.c,143 :: 		if (IOCAF3_bit)
	BTFSS      IOCAF3_bit+0, BitPos(IOCAF3_bit+0)
	GOTO       L_interrupt0
;PulseSensorRF.c,145 :: 		IOCAF3_bit=0;
	BCF        IOCAF3_bit+0, BitPos(IOCAF3_bit+0)
;PulseSensorRF.c,146 :: 		if (SLEEP_IO==1){SLEEP_IO=0; LED=0;
	BTFSS      LATA4_bit+0, BitPos(LATA4_bit+0)
	GOTO       L_interrupt1
	BCF        LATA4_bit+0, BitPos(LATA4_bit+0)
	BCF        LATC3_bit+0, BitPos(LATC3_bit+0)
;PulseSensorRF.c,147 :: 		TMR1ON_bit=0;         // ????. Timer
	BCF        TMR1ON_bit+0, BitPos(TMR1ON_bit+0)
;PulseSensorRF.c,152 :: 		asm sleep;asm reset;} else {SLEEP_IO=1;  LED=1;TimeTick=0; Bluetooth_activ=0; }           // TimeTick=0;  SLEEP_WP=1;
	SLEEP
	RESET
	GOTO       L_interrupt2
L_interrupt1:
	BSF        LATA4_bit+0, BitPos(LATA4_bit+0)
	BSF        LATC3_bit+0, BitPos(LATC3_bit+0)
	CLRF       _TimeTick+0
	CLRF       _TimeTick+1
	CLRF       _TimeTick+2
	CLRF       _TimeTick+3
	BCF        _Bluetooth_activ+0, BitPos(_Bluetooth_activ+0)
L_interrupt2:
;PulseSensorRF.c,154 :: 		}
L_interrupt0:
;PulseSensorRF.c,156 :: 		if (TMR0IF_bit)
	BTFSS      TMR0IF_bit+0, BitPos(TMR0IF_bit+0)
	GOTO       L_interrupt3
;PulseSensorRF.c,159 :: 		TMR0=50;
	MOVLW      50
	MOVWF      TMR0+0
;PulseSensorRF.c,160 :: 		if (ledTick>5000)
	MOVF       _ledTick+1, 0
	SUBLW      19
	BTFSS      STATUS+0, 2
	GOTO       L__interrupt38
	MOVF       _ledTick+0, 0
	SUBLW      136
L__interrupt38:
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt4
;PulseSensorRF.c,162 :: 		LED=~LED;
	MOVLW
	XORWF      LATC3_bit+0, 1
;PulseSensorRF.c,163 :: 		ledTick=0;
	CLRF       _ledTick+0
	CLRF       _ledTick+1
;PulseSensorRF.c,164 :: 		}
L_interrupt4:
;PulseSensorRF.c,165 :: 		TMR0IF_bit=0;
	BCF        TMR0IF_bit+0, BitPos(TMR0IF_bit+0)
;PulseSensorRF.c,166 :: 		TimeTick++;
	MOVLW      1
	ADDWF      _TimeTick+0, 1
	MOVLW      0
	ADDWFC     _TimeTick+1, 1
	ADDWFC     _TimeTick+2, 1
	ADDWFC     _TimeTick+3, 1
;PulseSensorRF.c,167 :: 		ledTick++;
	INCF       _ledTick+0, 1
	BTFSC      STATUS+0, 2
	INCF       _ledTick+1, 1
;PulseSensorRF.c,168 :: 		}
L_interrupt3:
;PulseSensorRF.c,169 :: 		if (TMR1IF_bit)
	BTFSS      TMR1IF_bit+0, BitPos(TMR1IF_bit+0)
	GOTO       L_interrupt5
;PulseSensorRF.c,171 :: 		TMR1IF_bit = 0;
	BCF        TMR1IF_bit+0, BitPos(TMR1IF_bit+0)
;PulseSensorRF.c,172 :: 		TMR1 = Timer1_200Hz;
	MOVLW      24
	MOVWF      TMR1+0
	MOVLW      246
	MOVWF      TMR1+1
;PulseSensorRF.c,173 :: 		if (OutDataReady) return;
	MOVF       _OutDataReady+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_interrupt6
	GOTO       L__interrupt37
L_interrupt6:
;PulseSensorRF.c,174 :: 		ADGO_bit=1;
	BSF        ADGO_bit+0, BitPos(ADGO_bit+0)
;PulseSensorRF.c,175 :: 		while(ADGO_bit);
L_interrupt7:
	BTFSS      ADGO_bit+0, BitPos(ADGO_bit+0)
	GOTO       L_interrupt8
	GOTO       L_interrupt7
L_interrupt8:
;PulseSensorRF.c,177 :: 		filter_buff[filter_ndx] = ADC_Get_Sample(0);
	MOVF       _filter_ndx+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _filter_buff+0
	ADDWF      R0, 0
	MOVWF      FLOC__interrupt+0
	MOVLW      hi_addr(_filter_buff+0)
	ADDWFC     R1, 0
	MOVWF      FLOC__interrupt+1
	CLRF       FARG_ADC_Get_Sample_channel+0
	CALL       _ADC_Get_Sample+0
	MOVF       FLOC__interrupt+0, 0
	MOVWF      FSR1L
	MOVF       FLOC__interrupt+1, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,178 :: 		filter_ndx++;
	INCF       _filter_ndx+0, 1
;PulseSensorRF.c,179 :: 		if (filter_ndx >= FILTER_LENGTH) filter_ndx=0;
	MOVLW      10
	SUBWF      _filter_ndx+0, 0
	BTFSS      STATUS+0, 0
	GOTO       L_interrupt9
	CLRF       _filter_ndx+0
L_interrupt9:
;PulseSensorRF.c,180 :: 		Val = 0;
	CLRF       _Val+0
	CLRF       _Val+1
;PulseSensorRF.c,181 :: 		for(i=0;i<FILTER_LENGTH;i++) Val += filter_buff[i];
	CLRF       _i+0
L_interrupt10:
	MOVLW      10
	SUBWF      _i+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_interrupt11
	MOVF       _i+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _filter_buff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_filter_buff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	ADDWF      _Val+0, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	ADDWFC     _Val+1, 1
	INCF       _i+0, 1
	GOTO       L_interrupt10
L_interrupt11:
;PulseSensorRF.c,182 :: 		OutDataBuff[OutCounter] = Val/FILTER_LENGTH;
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutDataBuff+0
	ADDWF      R0, 0
	MOVWF      FLOC__interrupt+0
	MOVLW      hi_addr(_OutDataBuff+0)
	ADDWFC     R1, 0
	MOVWF      FLOC__interrupt+1
	MOVLW      10
	MOVWF      R4
	MOVLW      0
	MOVWF      R5
	MOVF       _Val+0, 0
	MOVWF      R0
	MOVF       _Val+1, 0
	MOVWF      R1
	CALL       _Div_16x16_U+0
	MOVF       FLOC__interrupt+0, 0
	MOVWF      FSR1L
	MOVF       FLOC__interrupt+1, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,183 :: 		OutAsciiBuff[OutCounter]=0x00000000;
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      FSR1L
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR1H
	CLRF       INDF1+0
	ADDFSR     1, 1
	CLRF       INDF1+0
	ADDFSR     1, 1
	CLRF       INDF1+0
	ADDFSR     1, 1
	CLRF       INDF1+0
;PulseSensorRF.c,184 :: 		OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>12)&0x000F]<<24;
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      R9
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      R10
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutDataBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutDataBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R3
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R4
	MOVLW      12
	MOVWF      R2
	MOVF       R3, 0
	MOVWF      R0
	MOVF       R4, 0
	MOVWF      R1
	MOVF       R2, 0
L__interrupt39:
	BTFSC      STATUS+0, 2
	GOTO       L__interrupt40
	LSRF       R1, 1
	RRF        R0, 1
	ADDLW      255
	GOTO       L__interrupt39
L__interrupt40:
	MOVLW      15
	ANDWF      R0, 1
	MOVLW      0
	ANDWF      R1, 1
	MOVLW      _ByteToAscii+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_ByteToAscii+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	CLRF       R6
	CLRF       R7
	CLRF       R8
	MOVF       R5, 0
	MOVWF      R3
	CLRF       R0
	CLRF       R1
	CLRF       R2
	MOVF       R9, 0
	MOVWF      FSR0L
	MOVF       R10, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	IORWF       R0, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R1, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R2, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R3, 1
	MOVF       R9, 0
	MOVWF      FSR1L
	MOVF       R10, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R2, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R3, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,185 :: 		OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>8)&0x000F]<<16;
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      R9
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      R10
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutDataBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutDataBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R3
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R4
	MOVF       R4, 0
	MOVWF      R0
	CLRF       R1
	MOVLW      15
	ANDWF      R0, 1
	MOVLW      0
	ANDWF      R1, 1
	MOVLW      _ByteToAscii+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_ByteToAscii+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	CLRF       R6
	CLRF       R7
	CLRF       R8
	MOVF       R6, 0
	MOVWF      R3
	MOVF       R5, 0
	MOVWF      R2
	CLRF       R0
	CLRF       R1
	MOVF       R9, 0
	MOVWF      FSR0L
	MOVF       R10, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	IORWF       R0, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R1, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R2, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R3, 1
	MOVF       R9, 0
	MOVWF      FSR1L
	MOVF       R10, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R2, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R3, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,186 :: 		OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[(OutDataBuff[OutCounter]>>4)&0x000F]<<8;
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      R9
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      R10
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutDataBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutDataBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R3
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R4
	MOVF       R3, 0
	MOVWF      R0
	MOVF       R4, 0
	MOVWF      R1
	LSRF       R1, 1
	RRF        R0, 1
	LSRF       R1, 1
	RRF        R0, 1
	LSRF       R1, 1
	RRF        R0, 1
	LSRF       R1, 1
	RRF        R0, 1
	MOVLW      15
	ANDWF      R0, 1
	MOVLW      0
	ANDWF      R1, 1
	MOVLW      _ByteToAscii+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_ByteToAscii+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	CLRF       R6
	CLRF       R7
	CLRF       R8
	MOVF       R7, 0
	MOVWF      R3
	MOVF       R6, 0
	MOVWF      R2
	MOVF       R5, 0
	MOVWF      R1
	CLRF       R0
	MOVF       R9, 0
	MOVWF      FSR0L
	MOVF       R10, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	IORWF       R0, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R1, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R2, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R3, 1
	MOVF       R9, 0
	MOVWF      FSR1L
	MOVF       R10, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R2, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R3, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,187 :: 		OutAsciiBuff[OutCounter]|=(DWORD)ByteToAscii[OutDataBuff[OutCounter]&0x000F];
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      R4
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      R5
	MOVF       _OutCounter+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutDataBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutDataBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVLW      15
	ANDWF      INDF0+0, 0
	MOVWF      R0
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R1
	MOVLW      0
	ANDWF      R1, 1
	MOVLW      _ByteToAscii+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_ByteToAscii+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R0
	CLRF       R1
	CLRF       R2
	CLRF       R3
	MOVF       R4, 0
	MOVWF      FSR0L
	MOVF       R5, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	IORWF       R0, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R1, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R2, 1
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	IORWF       R3, 1
	MOVF       R4, 0
	MOVWF      FSR1L
	MOVF       R5, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
	MOVF       R1, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R2, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
	MOVF       R3, 0
	ADDFSR     1, 1
	MOVWF      INDF1+0
;PulseSensorRF.c,188 :: 		OutCounter++;
	INCF       _OutCounter+0, 1
;PulseSensorRF.c,189 :: 		if (OutCounter==OutDataSize)
	MOVF       _OutCounter+0, 0
	XORLW      10
	BTFSS      STATUS+0, 2
	GOTO       L_interrupt13
;PulseSensorRF.c,191 :: 		OutCounter=0;
	CLRF       _OutCounter+0
;PulseSensorRF.c,192 :: 		OutDataReady=1;
	MOVLW      1
	MOVWF      _OutDataReady+0
;PulseSensorRF.c,193 :: 		}
L_interrupt13:
;PulseSensorRF.c,194 :: 		}
L_interrupt5:
;PulseSensorRF.c,195 :: 		}
L_end_interrupt:
L__interrupt37:
	RETFIE     %s
; end of _interrupt

_RecieveData:

;PulseSensorRF.c,197 :: 		void RecieveData()
;PulseSensorRF.c,199 :: 		if (UART1_Data_Ready())
	CALL       _UART1_Data_Ready+0
	MOVF       R0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_RecieveData14
;PulseSensorRF.c,200 :: 		if (StartDetected)
	MOVF       _StartDetected+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_RecieveData15
;PulseSensorRF.c,202 :: 		InBuff[Counter] = UART1_Read();
	MOVLW      _InBuff+0
	MOVWF      FLOC__RecieveData+0
	MOVLW      hi_addr(_InBuff+0)
	MOVWF      FLOC__RecieveData+1
	MOVF       _Counter+0, 0
	ADDWF      FLOC__RecieveData+0, 1
	BTFSC      STATUS+0, 0
	INCF       FLOC__RecieveData+1, 1
	CALL       _UART1_Read+0
	MOVF       FLOC__RecieveData+0, 0
	MOVWF      FSR1L
	MOVF       FLOC__RecieveData+1, 0
	MOVWF      FSR1H
	MOVF       R0, 0
	MOVWF      INDF1+0
;PulseSensorRF.c,203 :: 		if (InBuff[Counter]==StopByte)
	MOVLW      _InBuff+0
	MOVWF      FSR0L
	MOVLW      hi_addr(_InBuff+0)
	MOVWF      FSR0H
	MOVF       _Counter+0, 0
	ADDWF      FSR0L, 1
	BTFSC      STATUS+0, 0
	INCF       FSR0H, 1
	MOVF       INDF0+0, 0
	XORLW      13
	BTFSS      STATUS+0, 2
	GOTO       L_RecieveData16
;PulseSensorRF.c,205 :: 		for(i=0;i<Counter/2;i++){
	CLRF       _i+0
L_RecieveData17:
	MOVF       _Counter+0, 0
	MOVWF      R1
	LSRF       R1, 1
	MOVF       R1, 0
	SUBWF      _i+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_RecieveData18
;PulseSensorRF.c,206 :: 		Dummy = AsciiToByte[InBuff[i*2+1]];
	MOVF       _i+0, 0
	MOVWF      R2
	CLRF       R3
	LSLF       R2, 1
	RLF        R3, 1
	MOVLW      1
	ADDWF      R2, 0
	MOVWF      R0
	MOVLW      0
	ADDWFC     R3, 0
	MOVWF      R1
	MOVLW      _InBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_InBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R0
	MOVLW      _AsciiToByte+0
	MOVWF      FSR0L
	MOVLW      hi_addr(_AsciiToByte+0)
	MOVWF      FSR0H
	MOVF       R0, 0
	ADDWF      FSR0L, 1
	BTFSC      STATUS+0, 0
	INCF       FSR0H, 1
	MOVF       INDF0+0, 0
	MOVWF      R4
	MOVF       R4, 0
	MOVWF      _Dummy+0
;PulseSensorRF.c,207 :: 		Dummy |= AsciiToByte[InBuff[i*2]]<<4;
	MOVLW      _InBuff+0
	ADDWF      R2, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_InBuff+0)
	ADDWFC     R3, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R0
	MOVLW      _AsciiToByte+0
	MOVWF      FSR0L
	MOVLW      hi_addr(_AsciiToByte+0)
	MOVWF      FSR0H
	MOVF       R0, 0
	ADDWF      FSR0L, 1
	BTFSC      STATUS+0, 0
	INCF       FSR0H, 1
	MOVF       INDF0+0, 0
	MOVWF      R2
	MOVF       R2, 0
	MOVWF      R0
	LSLF       R0, 1
	LSLF       R0, 1
	LSLF       R0, 1
	LSLF       R0, 1
	MOVF       R4, 0
	IORWF       R0, 1
	MOVF       R0, 0
	MOVWF      _Dummy+0
;PulseSensorRF.c,208 :: 		InData[i] = Dummy;
	MOVLW      _InData+0
	MOVWF      FSR1L
	MOVLW      hi_addr(_InData+0)
	MOVWF      FSR1H
	MOVF       _i+0, 0
	ADDWF      FSR1L, 1
	BTFSC      STATUS+0, 0
	INCF       FSR1H, 1
	MOVF       R0, 0
	MOVWF      INDF1+0
;PulseSensorRF.c,205 :: 		for(i=0;i<Counter/2;i++){
	INCF       _i+0, 1
;PulseSensorRF.c,209 :: 		}
	GOTO       L_RecieveData17
L_RecieveData18:
;PulseSensorRF.c,210 :: 		StartDetected=0;
	CLRF       _StartDetected+0
;PulseSensorRF.c,211 :: 		InDataReady=1;
	MOVLW      1
	MOVWF      _InDataReady+0
;PulseSensorRF.c,212 :: 		}
L_RecieveData16:
;PulseSensorRF.c,213 :: 		Counter++;
	INCF       _Counter+0, 1
;PulseSensorRF.c,214 :: 		} else{
	GOTO       L_RecieveData20
L_RecieveData15:
;PulseSensorRF.c,216 :: 		Dummy = UART1_Read();
	CALL       _UART1_Read+0
	MOVF       R0, 0
	MOVWF      _Dummy+0
;PulseSensorRF.c,217 :: 		if(Dummy==StartByte)
	MOVF       R0, 0
	XORLW      58
	BTFSS      STATUS+0, 2
	GOTO       L_RecieveData21
;PulseSensorRF.c,219 :: 		StartDetected=1;
	MOVLW      1
	MOVWF      _StartDetected+0
;PulseSensorRF.c,220 :: 		Counter=0;
	CLRF       _Counter+0
;PulseSensorRF.c,221 :: 		}
L_RecieveData21:
;PulseSensorRF.c,222 :: 		}
L_RecieveData20:
L_RecieveData14:
;PulseSensorRF.c,223 :: 		}
L_end_RecieveData:
	RETURN
; end of _RecieveData

_SendData:

;PulseSensorRF.c,224 :: 		void SendData()
;PulseSensorRF.c,226 :: 		UART1_Write(StartByte);
	MOVLW      58
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,228 :: 		UART1_Write(ByteToAscii[PulseData >> 4]);
	MOVF       _ByteToAscii+0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,229 :: 		UART1_Write(ByteToAscii[PulseData & 0x0F]);
	MOVF       _ByteToAscii+7, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,231 :: 		for(i=0;i<OutDataSize;i++)
	CLRF       _i+0
L_SendData22:
	MOVLW      10
	SUBWF      _i+0, 0
	BTFSC      STATUS+0, 0
	GOTO       L_SendData23
;PulseSensorRF.c,233 :: 		UART1_Write(OutAsciiBuff[i]>>24);
	MOVF       _i+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R6
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R7
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R8
	MOVF       R8, 0
	MOVWF      R0
	CLRF       R1
	CLRF       R2
	CLRF       R3
	MOVF       R0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,234 :: 		UART1_Write(OutAsciiBuff[i]>>16);
	MOVF       _i+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R6
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R7
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R8
	MOVF       R7, 0
	MOVWF      R0
	MOVF       R8, 0
	MOVWF      R1
	CLRF       R2
	CLRF       R3
	MOVF       R0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,235 :: 		UART1_Write(OutAsciiBuff[i]>>8);
	MOVF       _i+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      R5
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R6
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R7
	ADDFSR     0, 1
	MOVF       INDF0+0, 0
	MOVWF      R8
	MOVF       R6, 0
	MOVWF      R0
	MOVF       R7, 0
	MOVWF      R1
	MOVF       R8, 0
	MOVWF      R2
	CLRF       R3
	MOVF       R0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,236 :: 		UART1_Write(OutAsciiBuff[i]);
	MOVF       _i+0, 0
	MOVWF      R0
	CLRF       R1
	LSLF       R0, 1
	RLF        R1, 1
	LSLF       R0, 1
	RLF        R1, 1
	MOVLW      _OutAsciiBuff+0
	ADDWF      R0, 0
	MOVWF      FSR0L
	MOVLW      hi_addr(_OutAsciiBuff+0)
	ADDWFC     R1, 0
	MOVWF      FSR0H
	MOVF       INDF0+0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,231 :: 		for(i=0;i<OutDataSize;i++)
	INCF       _i+0, 1
;PulseSensorRF.c,237 :: 		}
	GOTO       L_SendData22
L_SendData23:
;PulseSensorRF.c,238 :: 		UART1_Write(StopByte);
	MOVLW      13
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,239 :: 		}
L_end_SendData:
	RETURN
; end of _SendData

_main:

;PulseSensorRF.c,241 :: 		void main() {
;PulseSensorRF.c,242 :: 		InitMain();
	CALL       _InitMain+0
;PulseSensorRF.c,243 :: 		StartDetected=0; Dummy=0; Counter=0;  InDataReady=0; OutCounter=0;filter_ndx=0;
	CLRF       _StartDetected+0
	CLRF       _Dummy+0
	CLRF       _Counter+0
	CLRF       _InDataReady+0
	CLRF       _OutCounter+0
	CLRF       _filter_ndx+0
;PulseSensorRF.c,244 :: 		TimeTick=0;   SLEEP_WP=0;  Bluetooth_activ=0;
	CLRF       _TimeTick+0
	CLRF       _TimeTick+1
	CLRF       _TimeTick+2
	CLRF       _TimeTick+3
	BCF        _SLEEP_WP+0, BitPos(_SLEEP_WP+0)
	BCF        _Bluetooth_activ+0, BitPos(_Bluetooth_activ+0)
;PulseSensorRF.c,252 :: 		while(1)
L_main25:
;PulseSensorRF.c,255 :: 		RecieveData();
	CALL       _RecieveData+0
;PulseSensorRF.c,256 :: 		if (InDataReady)
	MOVF       _InDataReady+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main27
;PulseSensorRF.c,257 :: 		{  switch(InData[0])
	GOTO       L_main28
;PulseSensorRF.c,268 :: 		case StartMeasuring:
L_main30:
;PulseSensorRF.c,269 :: 		Bluetooth_activ=1;
	BSF        _Bluetooth_activ+0, BitPos(_Bluetooth_activ+0)
;PulseSensorRF.c,270 :: 		TMR1ON_bit=1;         // ???. Timer
	BSF        TMR1ON_bit+0, BitPos(TMR1ON_bit+0)
;PulseSensorRF.c,271 :: 		ADON_bit=1;           // ???. ???
	BSF        ADON_bit+0, BitPos(ADON_bit+0)
;PulseSensorRF.c,272 :: 		DACEN_bit=1;          // ???. ???
	BSF        DACEN_bit+0, BitPos(DACEN_bit+0)
;PulseSensorRF.c,273 :: 		DAC1CON1 = DACOUTPUT; // 1.5 V ???
	MOVLW      127
	MOVWF      DAC1CON1+0
;PulseSensorRF.c,274 :: 		OPA1EN_bit=1;         // ???. ??
	BSF        OPA1EN_bit+0, BitPos(OPA1EN_bit+0)
;PulseSensorRF.c,276 :: 		break;
	GOTO       L_main29
;PulseSensorRF.c,277 :: 		case StopMeasuring:
L_main31:
;PulseSensorRF.c,279 :: 		TMR1ON_bit=0;         // ????. Timer
	BCF        TMR1ON_bit+0, BitPos(TMR1ON_bit+0)
;PulseSensorRF.c,280 :: 		ADON_bit=0;           // ????. ???
	BCF        ADON_bit+0, BitPos(ADON_bit+0)
;PulseSensorRF.c,281 :: 		DACEN_bit=0;          // ????. ???
	BCF        DACEN_bit+0, BitPos(DACEN_bit+0)
;PulseSensorRF.c,282 :: 		OPA1EN_bit=0;         // ????. ??
	BCF        OPA1EN_bit+0, BitPos(OPA1EN_bit+0)
;PulseSensorRF.c,283 :: 		break;
	GOTO       L_main29
;PulseSensorRF.c,285 :: 		case IdentifyDevice:
L_main32:
;PulseSensorRF.c,286 :: 		UART1_Write(StartByte);
	MOVLW      58
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,287 :: 		UART1_Write(ByteToAscii[IdentifyDevice >> 4]);
	MOVF       _ByteToAscii+0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,288 :: 		UART1_Write(ByteToAscii[IdentifyDevice & 0x0F]);
	MOVF       _ByteToAscii+3, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,289 :: 		UART1_Write(ByteToAscii[TPulseWaveSensor >> 4]);
	MOVF       _ByteToAscii+0, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,290 :: 		UART1_Write(ByteToAscii[TPulseWaveSensor & 0x0F]);
	MOVF       _ByteToAscii+3, 0
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,291 :: 		UART1_Write(StopByte);
	MOVLW      13
	MOVWF      FARG_UART1_Write_data_+0
	CALL       _UART1_Write+0
;PulseSensorRF.c,292 :: 		break;
	GOTO       L_main29
;PulseSensorRF.c,294 :: 		default: break;
L_main33:
	GOTO       L_main29
;PulseSensorRF.c,295 :: 		}
L_main28:
	MOVF       _InData+0, 0
	XORLW      1
	BTFSC      STATUS+0, 2
	GOTO       L_main30
	MOVF       _InData+0, 0
	XORLW      2
	BTFSC      STATUS+0, 2
	GOTO       L_main31
	MOVF       _InData+0, 0
	XORLW      3
	BTFSC      STATUS+0, 2
	GOTO       L_main32
	GOTO       L_main33
L_main29:
;PulseSensorRF.c,296 :: 		InDataReady=0;
	CLRF       _InDataReady+0
;PulseSensorRF.c,297 :: 		}
L_main27:
;PulseSensorRF.c,306 :: 		if (OutDataReady)
	MOVF       _OutDataReady+0, 0
	BTFSC      STATUS+0, 2
	GOTO       L_main34
;PulseSensorRF.c,308 :: 		OutDataReady=0;
	CLRF       _OutDataReady+0
;PulseSensorRF.c,309 :: 		SendData();
	CALL       _SendData+0
;PulseSensorRF.c,310 :: 		LED=~LED;
	MOVLW
	XORWF      LATC3_bit+0, 1
;PulseSensorRF.c,311 :: 		}
L_main34:
;PulseSensorRF.c,312 :: 		}
	GOTO       L_main25
;PulseSensorRF.c,314 :: 		}
L_end_main:
	GOTO       $+0
; end of _main
