#line 1 "D:/documents/Projects/PulseSensorRF/firmware/PulseSensorRF.c"
#line 38 "D:/documents/Projects/PulseSensorRF/firmware/PulseSensorRF.c"
 unsigned char  InBuff[16];
 unsigned char  InData[16];
 unsigned char  i,j, StartDetected, InDataReady,OutDataReady, Counter,WasStarted, SampleStepCounter, OutCounter;
 unsigned long  TimeTick;
 unsigned char  Unpressed_Count, Pressed_Count;
  unsigned long  a;
 unsigned int  OutDataBuff[ 10 ];
 unsigned int  filter_buff[ 10 ];
 unsigned char  filter_ndx;
 unsigned long  OutAsciiBuff[ 10 ];
 unsigned long  AverageBuff=0;
 unsigned int  Val;
 unsigned int  Average=0;
 unsigned int  AverageCounter=0;

 unsigned int  cnt,ledTick;
 unsigned char  uart_rd;

 unsigned char  Dummy;
 unsigned char  AsciiToByte[71] = {
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,
0,1,2,3,4,5,6,7,8,9,
0,0,0,0,0,0,0,
10,11,12,13,14,15};
 unsigned char  ByteToAscii[16] = {0x30,0x31,0x32,0x33,0x34,0x35,0x36,0x37,0x38,0x39,0x41,0x42,0x43,0x44,0x45,0x46};
bit oldstate;
bit SLEEP_WP;
bit Bluetooth_activ;

void InitMain (){


 OSCCON = 0x7A;

 TRISA = 0x0B;
 TRISC3_bit = 0;
 TRISC0_bit = 1;
 TRISC1_bit = 1;
 TRISC2_bit = 0;

 RC4PPS = 0x14;
 RXPPS = 0x15;

 ANSELA = 0x03;
 ADCON0 = 0x00;
 ADCON1 = 0x02;
 ADCON2 = 0x00;
 ADFM_bit=1;





 ANSELC = 0x07;
 OPA1CON = 0x00;


 DAC1CON0 = 0x14;

 PSA_bit=0;
 PS0_bit = 0;
 PS1_bit = 0;
 PS2_bit = 0;
 TMR0CS_bit=0;
 TMR0IE_bit=1;
 GIE_bit = 1;

 TMR1CS0_bit = 0;
 TMR1CS1_bit = 0;
 T1CKPS0_bit = 1;
 T1CKPS1_bit = 1;
 PEIE_bit=1;
 TMR1IE_bit=1;

 TX1STA = 0x00;
 BRGH_bit = 1;
 TXEN_bit=1;
 RC1STA=0x00;
 SREN_bit=1;
 SPEN_bit=1;

 SP1BRGL = 8;

 UART1_Init(115200);

  LATC3_bit =1;


  LATA4_bit =1;

 LATC3_bit=0;
 TRISA = 0x0B;
 oldstate=0;

 IOCAP3_bit=1;
 IOCIE_bit=1;


}
void interrupt ()
{
 if (IOCAF3_bit)
 {
 IOCAF3_bit=0;
 if ( LATA4_bit ==1){ LATA4_bit =0;  LATC3_bit =0;
 TMR1ON_bit=0;




 asm sleep;asm reset;} else { LATA4_bit =1;  LATC3_bit =1;TimeTick=0; Bluetooth_activ=0; }

 }

 if (TMR0IF_bit)
 {

 TMR0=50;
 if (ledTick>5000)
 {
  LATC3_bit =~ LATC3_bit ;
 ledTick=0;
 }
 TMR0IF_bit=0;
 TimeTick++;
 ledTick++;
 }
 if (TMR1IF_bit)
 {
 TMR1IF_bit = 0;
 TMR1 =  63000 ;
 if (OutDataReady) return;
 ADGO_bit=1;
 while(ADGO_bit);

 filter_buff[filter_ndx] = ADC_Get_Sample(0);
 filter_ndx++;
 if (filter_ndx >=  10 ) filter_ndx=0;
 Val = 0;
 for(i=0;i< 10 ;i++) Val += filter_buff[i];
 OutDataBuff[OutCounter] = Val/ 10 ;
 OutAsciiBuff[OutCounter]=0x00000000;
 OutAsciiBuff[OutCounter]|=( unsigned long )ByteToAscii[(OutDataBuff[OutCounter]>>12)&0x000F]<<24;
 OutAsciiBuff[OutCounter]|=( unsigned long )ByteToAscii[(OutDataBuff[OutCounter]>>8)&0x000F]<<16;
 OutAsciiBuff[OutCounter]|=( unsigned long )ByteToAscii[(OutDataBuff[OutCounter]>>4)&0x000F]<<8;
 OutAsciiBuff[OutCounter]|=( unsigned long )ByteToAscii[OutDataBuff[OutCounter]&0x000F];
 OutCounter++;
 if (OutCounter== 10 )
 {
 OutCounter=0;
 OutDataReady=1;
 }
 }
}

void RecieveData()
{
 if (UART1_Data_Ready())
 if (StartDetected)
 {
 InBuff[Counter] = UART1_Read();
 if (InBuff[Counter]== 0x0D )
 {
 for(i=0;i<Counter/2;i++){
 Dummy = AsciiToByte[InBuff[i*2+1]];
 Dummy |= AsciiToByte[InBuff[i*2]]<<4;
 InData[i] = Dummy;
 }
 StartDetected=0;
 InDataReady=1;
 }
 Counter++;
 } else{

 Dummy = UART1_Read();
 if(Dummy== 0x3A )
 {
 StartDetected=1;
 Counter=0;
 }
 }
}
void SendData()
{
 UART1_Write( 0x3A );

 UART1_Write(ByteToAscii[ 0x07  >> 4]);
 UART1_Write(ByteToAscii[ 0x07  & 0x0F]);

 for(i=0;i< 10 ;i++)
 {
 UART1_Write(OutAsciiBuff[i]>>24);
 UART1_Write(OutAsciiBuff[i]>>16);
 UART1_Write(OutAsciiBuff[i]>>8);
 UART1_Write(OutAsciiBuff[i]);
 }
 UART1_Write( 0x0D );
}

void main() {
 InitMain();
 StartDetected=0; Dummy=0; Counter=0; InDataReady=0; OutCounter=0;filter_ndx=0;
 TimeTick=0; SLEEP_WP=0; Bluetooth_activ=0;
#line 252 "D:/documents/Projects/PulseSensorRF/firmware/PulseSensorRF.c"
 while(1)
 {

 RecieveData();
 if (InDataReady)
 { switch(InData[0])
 {









 case  0x01 :
 Bluetooth_activ=1;
 TMR1ON_bit=1;
 ADON_bit=1;
 DACEN_bit=1;
 DAC1CON1 =  127 ;
 OPA1EN_bit=1;

 break;
 case  0x02 :

 TMR1ON_bit=0;
 ADON_bit=0;
 DACEN_bit=0;
 OPA1EN_bit=0;
 break;

 case  0x03 :
 UART1_Write( 0x3A );
 UART1_Write(ByteToAscii[ 0x03  >> 4]);
 UART1_Write(ByteToAscii[ 0x03  & 0x0F]);
 UART1_Write(ByteToAscii[ 0x03  >> 4]);
 UART1_Write(ByteToAscii[ 0x03  & 0x0F]);
 UART1_Write( 0x0D );
 break;

 default: break;
 }
 InDataReady=0;
 }
#line 306 "D:/documents/Projects/PulseSensorRF/firmware/PulseSensorRF.c"
 if (OutDataReady)
 {
 OutDataReady=0;
 SendData();
  LATC3_bit =~ LATC3_bit ;
 }
 }

}
